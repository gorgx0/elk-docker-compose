#!/usr/bin/env bash

mkdir -p elasticsearch/data
mkdir -p elasticsearch/logs
mkdir -p logstash/data

chown 1000 elasticsearch/data elasticsearch/logs
